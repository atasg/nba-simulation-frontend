import axios from "axios"

export const state = () => ({
    initialMatches: [
        {
            "name": "Match 1",
            "firstteam": "Toronto Raptors",
            "secondteam": "Miami Heat"
        },
        {
            "name": "Match 2",
            "firstteam": "Boston Celtics",
            "secondteam": "Chicago Bulls"
        },
        {
            "name": "Match 3",
            "firstteam": "Houston Rockets",
            "secondteam": "Detroit Pistons"
        },
        {
            "name": "Match 4",
            "firstteam": "Los Angeles Lakers",
            "secondteam": "Brooklyn Nets"
        },
        {
            "name": "Match 5",
            "firstteam": "Dallas Mavericks",
            "secondteam": "Cleveland Cavaliers"
        }
    ],
})

export const mutations = {
    startGame(state) {

    }
}

export const actions = {
    async startGame({commit}, payload) {
        axios.post("http://127.0.0.1:8080/matches", payload)
    }
}
